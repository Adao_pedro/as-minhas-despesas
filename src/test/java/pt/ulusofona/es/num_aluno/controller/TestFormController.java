package pt.ulusofona.es.num_aluno.controller;

import com.sun.security.auth.UserPrincipal;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pt.ulusofona.es.num_aluno.data.Despesa;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/main/webapp/WEB-INF/dispatcher-servlet-test.xml")
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class TestFormController {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void testListaDespesasVazia() throws Exception {

        List<Despesa> expectedListaDespesas = new ArrayList<Despesa>();

        mvc.perform(get("/list").principal(new UserPrincipal("admin")))
                .andExpect(status().isOk())
                .andExpect(view().name("list"))
                .andExpect(model().attribute("despesas", expectedListaDespesas));
    }


    @Test
    public void testInsereDespesas() throws Exception {

        // dados do formulário


        String descricao = "compra passe";
        String localizacao = "Campo Grande";
     long data = 1;
        String login = "Pedro";
        String recorrencia = "Mensal";
        String expectedMessage = "Sucesso! A despesas " + descricao +
                " foi gravado na BD e foi-lhe atribuído o id 1";

        // POST do formulário


        // dados do utilizador que vai ser inserido na BD
        Despesa expectedDespesa = new Despesa();

        expectedDespesa.setDescricao(descricao);
        expectedDespesa.setLocalizacao(localizacao);

        expectedDespesa.setData(data);
        expectedDespesa.setLogin(login);
        expectedDespesa.setRecorrencia(recorrencia);

        List<Despesa> expectedListaDespesas = new ArrayList<Despesa>();
        expectedListaDespesas.add(expectedDespesa);

        // Obtém lista de utilizadores
        mvc.perform(get("/list").principal(new UserPrincipal("admin")))
                .andExpect(status().isOk())
                .andExpect(view().name("list"))
                .andExpect(model().attribute("despesas", expectedListaDespesas));

        // Obtém dados do utilizador inserido
        mvc.perform(get("/info/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("info"))
                .andExpect(model().attribute("despesa", expectedDespesa));
    }


}
