package pt.ulusofona.es.num_aluno.form;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.math.BigDecimal;

public class DespesaForm {

    private Long id;
    @NotNull (message="A categoria tem de estar preenchido")
    @Size(min=0, max=55, message="O nome da categoria tem que ter menos de 55 caracteres")
    private String  categoria;

    @Size(min=0, max=160, message="a descricao tem que ter menos de 160 caracteres")
    private String descricao;

    @NotNull(message = "O valor tem que estar registado")
    private Double  valor;

    @NotNull(message = "a localizacao tem que estar registada")
    private String localizacao;

    @NotNull(message = "a data tem que estar registada")
    private long data;

    @Column(nullable = false)
    private String login;

    @Size(min=0, max=55, message="O nome da categoria tem que ter menos de 55 caracteres")
    private String recorrencia;


    public void setId(Long id) {
        this.id = id;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Double getValor() {
        return valor;
    }


    public String getCategoria() {
        return categoria;
    }

    public long  getData() {
        return data;
    }

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public void setRecorrencia(String recorrencia) {
        this.recorrencia = recorrencia;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setData(long  data) {
        this.data = data;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }


    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getRecorrencia() {
        return recorrencia;
    }

}