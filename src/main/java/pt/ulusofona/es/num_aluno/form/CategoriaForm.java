package pt.ulusofona.es.num_aluno.form;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CategoriaForm {

    private Integer id;
    @NotNull (message="A categoria tem de estar preenchido")
    @Size(min=0, max=55, message="O nome da categoria tem que ter menos de 55 caracteres")
    private String  name;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
