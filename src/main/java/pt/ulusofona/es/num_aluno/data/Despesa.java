package pt.ulusofona.es.num_aluno.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Calendar;
@Entity
public class Despesa implements  Serializable
{

    @Id
    @GeneratedValue
    private Long id;

    @Column (nullable = false, length = 55)
    private String  categoria;


    @Column (nullable = false, length = 160)
    private String descricao;

    @Column (nullable = false)
    private Double valor;

    @Column (nullable = false)
    private String localizacao;

    @Column (nullable = false)
    private long  data;

    @Column(nullable = false)
    private String login;

    private String recorrencia;


    private transient String nomeUser;

    private transient String emailUser;

    private String categoriaName;

    private transient  String recorrenciaName;

    private  transient String dataName;

    public Long getId() {
        return id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescricao() {
        return descricao;
    }

    public Double getValor() {
        return valor;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public long  getData() {
        return data;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public void setData(long  data) {
        this.data = data;
    }
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNomeUser() {
        return nomeUser;
    }


    public void setNomeUser(String nomeUser) {
        this.nomeUser = nomeUser;
    }


    public String getEmailUser() {
        return emailUser;
    }

    public void setEmailUser(String emailUser) {
        this.emailUser = emailUser;
    }


    public String getCategoriaName() {
        return categoriaName;
    }

    public void setCategoriaName(String categoriaName) {
        this.categoriaName = categoriaName;
    }



    public void setRecorrencia(String recorrencia) {
        this.recorrencia = recorrencia;
    }

    public String  getRecorrencia(){
        return recorrencia;
    }

    public String getRecorrenciaName() {
        return recorrenciaName;
    }

    public void setRecorrenciaName(String recorrenciaName) {
        this.recorrenciaName = recorrenciaName;
    }

    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    public String getDataName() {
        return dataName;
    }
}
