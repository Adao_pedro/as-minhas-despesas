package pt.ulusofona.es.num_aluno.controller;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pt.ulusofona.es.num_aluno.data.Categoria;
import pt.ulusofona.es.num_aluno.data.Despesa;
import pt.ulusofona.es.num_aluno.data.Utilizador;
import pt.ulusofona.es.num_aluno.form.CategoriaForm;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
@Transactional
public class ApplicationContextListener implements ApplicationListener<ContextRefreshedEvent> {

    @PersistenceContext
    private EntityManager em;



    public void onApplicationEvent(ContextRefreshedEvent event) {




        List<Categoria> categorias = em.createQuery("select e from  Categoria e", Categoria.class).getResultList();


        if (categorias == null || categorias.isEmpty()) {
            // popular a BD com os estados civis possíveis




                em.persist(new Categoria(1, "Transporte"));
                em.persist(new Categoria(2, "Alimentação"));
                em.persist(new Categoria(3, "Propinas"));
                em.persist(new Categoria(4, "Rendas"));


        }





    }



}
