package pt.ulusofona.es.num_aluno.controller;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pt.ulusofona.es.num_aluno.data.Categoria;
import pt.ulusofona.es.num_aluno.data.Utilizador;
import pt.ulusofona.es.num_aluno.form.CategoriaForm;
import pt.ulusofona.es.num_aluno.form.UtilizadorForm;
import pt.ulusofona.es.num_aluno.data.Despesa;
import pt.ulusofona.es.num_aluno.form.DespesaForm;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.security.Principal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@Transactional


public class FormController {

    @PersistenceContext
    private EntityManager em;

    Utilizador utilizador;


////// Lista de despesas por utilizadores

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(ModelMap model, Principal user) {
        return "redirect:/form";
    }




    @RequestMapping(value = "/list", method = RequestMethod.GET)

    public String getList(ModelMap model, Principal user) {


        if (user.getName() != null){
            utilizador = em.find(Utilizador.class, user.getName());
        }


        // obtem lista de despesas da BD
        List<Despesa> despesas = em.createQuery("select u from Despesa u where u.login = :parametroUser ORDER BY u.data", Despesa.class ).
                setParameter("parametroUser", user.getName()).
                getResultList();






        Map categoriasMap = getcategoriaMap();


        for (Despesa despesa : despesas) {



            String despesaName = " ";
            String  dataNome ="";


            if(despesa.getData()==1){
                dataNome="Janeiro";
            }

            else if(despesa.getData()==2){
                dataNome="Fevereiro";
            }

            else if(despesa.getData()==3){
                dataNome="Março";
            }

            else if(despesa.getData()==4){
                dataNome="Abril";
            }
            else if(despesa.getData()==5){
                dataNome="Maio";
            }

            else if(despesa.getData()==6){
                dataNome="Junho";
            }

            else if(despesa.getData()==7){
                dataNome="Julho";
            }

            else if(despesa.getData()==8){
                dataNome="Agosto";
            }
            else if(despesa.getData()==9){
                dataNome="Setembro";
            }

            else if(despesa.getData()==10){
                dataNome="Outubro";
            }

            else if(despesa.getData()==11){
                dataNome="Novembro";
            }

            else if(despesa.getData()==12){
                dataNome="Dezembro";
            }

            else{
                dataNome="O Fc do Porto é o maior! ";
            }


                despesaName = despesa.getCategoria();



            despesa.setCategoriaName(despesaName);
            despesa.setDescricao(despesa.getDescricao());
            despesa.setValor(despesa.getValor());
            despesa.setLocalizacao(despesa.getLocalizacao());
            despesa.setData(despesa.getData());
            despesa.setLogin(user.getName());
            despesa.setDataName(dataNome);




        }


        // adiciona ao model
        model.put("despesas",despesas);

        return "list";
    }





    private Map<String,String> getcategoriaMap() {

        Map<String,String> categoriasMap = new HashMap<String,String>();

        // imaginando que existia uma tabela de despesas civis na BD
        List<Categoria> categorias = em.createQuery("select e from Categoria e", Categoria.class).getResultList();
        for (Categoria categoria : categorias) {
            categoriasMap.put(categoria.getName(),categoria.getName());
        }

        // ou se quisermos carregar directamente sem vir da BD
//     categoriasMap.put(1, "Solteiro");
//        categoriasMap.put(2, "Casado");
//        categoriasMap.put(3, "Divorciado");

        return categoriasMap;
    }



    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String getForm(ModelMap model, Principal user) {
        model.put("despesaForm", new DespesaForm());

        if(user.getName().equals("admin")){
            model.addAttribute("permissao", "admin");
        }


        // passar ao jsp os items que vão fazer parte da combobox do tipo
        model.put("categoriasMap", getcategoriaMap());

        return "form";
    }




    @RequestMapping(value = "/form", method = RequestMethod.POST)
    public String submitForm(@Valid @ModelAttribute("DespesaForm") DespesaForm despesaForm,
                             BindingResult bindingResult,
                             ModelMap model,Principal user) {

        if (bindingResult.hasErrors()) {
            // passar ao jsp os items que vão fazer parte da combobox do tipo
            model.put("categoriasMap", getcategoriaMap());
            return "form";
        }



        if (bindingResult.hasErrors()) {
            return "form";
        }



        Despesa despesa;
        if(despesaForm.getId() != null){
            despesa = em.find(Despesa.class, despesaForm.getId()); // se é um update
        }
        else{
            despesa = new Despesa();
        }
        despesa.setCategoria(despesaForm.getCategoria());
        despesa.setDescricao(despesaForm.getDescricao());
        despesa.setValor(despesaForm.getValor());
        despesa.setLocalizacao(despesaForm.getLocalizacao());
        despesa.setLogin(despesaForm.getLogin());
        despesa.setData(despesaForm.getData());
        despesa.setLogin(user.getName());
        despesa.setRecorrencia(despesaForm.getRecorrencia());

        em.persist(despesa);

        if (despesaForm.getRecorrencia().equals("Mensal")|| despesa.getRecorrencia().equals("Mensal")) {

            if (despesaForm.getData() != 1) {
                Despesa despesa1 = new Despesa();
                despesa1.setCategoria(despesaForm.getCategoria());
                despesa1.setDescricao(despesaForm.getDescricao());
                despesa1.setValor(despesaForm.getValor());
                despesa1.setLocalizacao(despesaForm.getLocalizacao());
                despesa1.setLogin(despesaForm.getLogin());
                despesa1.setData(1);
                despesa1.setLogin(user.getName());
                em.persist(despesa1);
            }

            if (despesaForm.getData() != 2) {
                Despesa despesa2 = new Despesa();
                despesa2.setCategoria(despesaForm.getCategoria());
                despesa2.setDescricao(despesaForm.getDescricao());
                despesa2.setValor(despesaForm.getValor());
                despesa2.setLocalizacao(despesaForm.getLocalizacao());
                despesa2.setLogin(despesaForm.getLogin());
                despesa2.setData(2);
                despesa2.setLogin(user.getName());
                em.persist(despesa2);
            }

            if (despesaForm.getData() != 3) {
                Despesa despesa3 = new Despesa();
                despesa3.setCategoria(despesaForm.getCategoria());
                despesa3.setDescricao(despesaForm.getDescricao());
                despesa3.setValor(despesaForm.getValor());
                despesa3.setLocalizacao(despesaForm.getLocalizacao());
                despesa3.setLogin(despesaForm.getLogin());
                despesa3.setData(3);
                despesa3.setLogin(user.getName());

                em.persist(despesa3);

            }

            if (despesaForm.getData() != 4) {
                Despesa despesa4 = new Despesa();
                despesa4.setCategoria(despesaForm.getCategoria());
                despesa4.setDescricao(despesaForm.getDescricao());
                despesa4.setValor(despesaForm.getValor());
                despesa4.setLocalizacao(despesaForm.getLocalizacao());
                despesa4.setLogin(despesaForm.getLogin());
                despesa4.setData(4);
                despesa4.setLogin(user.getName());

                em.persist(despesa4);

            }


            if (despesaForm.getData() != 5) {
                Despesa despesa5 = new Despesa();
                despesa5.setCategoria(despesaForm.getCategoria());
                despesa5.setDescricao(despesaForm.getDescricao());
                despesa5.setValor(despesaForm.getValor());
                despesa5.setLocalizacao(despesaForm.getLocalizacao());
                despesa5.setLogin(despesaForm.getLogin());
                despesa5.setData(5);
                despesa5.setLogin(user.getName());
                em.persist(despesa5);

            }

            if (despesaForm.getData() != 6) {
                Despesa despesa6 = new Despesa();
                despesa6.setCategoria(despesaForm.getCategoria());
                despesa6.setDescricao(despesaForm.getDescricao());
                despesa6.setValor(despesaForm.getValor());
                despesa6.setLocalizacao(despesaForm.getLocalizacao());
                despesa6.setLogin(despesaForm.getLogin());
                despesa6.setData(6);
                despesa6.setLogin(user.getName());

                em.persist(despesa6);

            }


            if (despesaForm.getData() != 7) {
                Despesa despesa7 = new Despesa();
                despesa7.setCategoria(despesaForm.getCategoria());
                despesa7.setDescricao(despesaForm.getDescricao());
                despesa7.setValor(despesaForm.getValor());
                despesa7.setLocalizacao(despesaForm.getLocalizacao());
                despesa7.setLogin(despesaForm.getLogin());
                despesa7.setData(7);
                despesa7.setLogin(user.getName());

                em.persist(despesa7);

            }

            if (despesaForm.getData() != 8) {
                Despesa despesa8 = new Despesa();
                despesa8.setCategoria(despesaForm.getCategoria());
                despesa8.setDescricao(despesaForm.getDescricao());
                despesa8.setValor(despesaForm.getValor());
                despesa8.setLocalizacao(despesaForm.getLocalizacao());
                despesa8.setLogin(despesaForm.getLogin());
                despesa8.setData(8);
                despesa8.setLogin(user.getName());

                em.persist(despesa8);

            }


            if (despesaForm.getData() != 9) {
                Despesa despesa9 = new Despesa();
                despesa9.setCategoria(despesaForm.getCategoria());
                despesa9.setDescricao(despesaForm.getDescricao());
                despesa9.setValor(despesaForm.getValor());
                despesa9.setLocalizacao(despesaForm.getLocalizacao());
                despesa9.setLogin(despesaForm.getLogin());
                despesa9.setData(9);
                despesa9.setLogin(user.getName());

                em.persist(despesa9);
            }


            if (despesaForm.getData() != 10) {
                Despesa despesa10 = new Despesa();
                despesa10.setCategoria(despesaForm.getCategoria());
                despesa10.setDescricao(despesaForm.getDescricao());
                despesa10.setValor(despesaForm.getValor());
                despesa10.setLocalizacao(despesaForm.getLocalizacao());
                despesa10.setLogin(despesaForm.getLogin());
                despesa10.setData(10);
                despesa10.setLogin(user.getName());

                em.persist(despesa10);

            }


            if (despesaForm.getData() != 11) {
                Despesa despesa11 = new Despesa();
                despesa11.setCategoria(despesaForm.getCategoria());
                despesa11.setDescricao(despesaForm.getDescricao());
                despesa11.setValor(despesaForm.getValor());
                despesa11.setLocalizacao(despesaForm.getLocalizacao());
                despesa11.setLogin(despesaForm.getLogin());
                despesa11.setData(11);
                despesa11.setLogin(user.getName());

                em.persist(despesa11);
            }

            if (despesaForm.getData() != 12) {
                Despesa despesa12 = new Despesa();
                despesa12.setCategoria(despesaForm.getCategoria());
                despesa12.setDescricao(despesaForm.getDescricao());
                despesa12.setValor(despesaForm.getValor());
                despesa12.setLocalizacao(despesaForm.getLocalizacao());
                despesa12.setLogin(despesaForm.getLogin());
                despesa12.setData(12);
                despesa12.setLogin(user.getName());
                em.persist(despesa12);
            }

        }




        if(despesaForm.getId() != null) {
            model.addAttribute("message", "Sucesso! A despesa  foi editado");
        }
        else {
            model.addAttribute("message", "Sucesso! A despesa  foi criado");
        }






        model.addAttribute("message", "Sucesso! A despesa " + despesa.getCategoria() +
                " foi gravado na BD com o valo de ");




        return "result";



    }



    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET)
    public String getInfo(ModelMap model, @PathVariable("id") Long id, Principal user) {

        Despesa despesa = em.find(Despesa.class, id);

        String despesaName = " ";




        String dataNome ="";


        if(despesa.getData()==1){
            dataNome="Janeiro";
        }

        else if(despesa.getData()==2){
            dataNome="Fevereiro";
        }

        else if(despesa.getData()==3){
            dataNome="Março";
        }

        else if(despesa.getData()==4){
            dataNome="Abril";
        }
        else if(despesa.getData()==5){
            dataNome="Maio";
        }

        else if(despesa.getData()==6){
            dataNome="Junho";
        }

        else if(despesa.getData()==7){
            dataNome="Julho";
        }

        else if(despesa.getData()==8){
            dataNome="Agosto";
        }
        else if(despesa.getData()==9){
            dataNome="Setembro";
        }

        else if(despesa.getData()==10){
            dataNome="Outubro";
        }

        else if(despesa.getData()==11){
            dataNome="Novembro";
        }

        else if(despesa.getData()==12){
            dataNome="Dezembro";
        }

        else{
            dataNome="O Fc do Porto é o maior! ";
        }

        despesaName = despesa.getCategoria();

        despesa.setCategoriaName(despesaName);

        despesa.setDataName( dataNome);




        model.put("despesa", despesa);

        if(user.getName().equals("admin")){
            model.addAttribute("permissao", "admin");
        }


        return "info";
    }




    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String edit_despesa(ModelMap model, @PathVariable("id") Long id){

        Despesa despesa = em.find(Despesa.class, id);
        DespesaForm despesaForm = new DespesaForm();
        despesaForm.setId(despesa.getId());
        despesaForm.setCategoria(despesa.getCategoria());
        despesaForm.setDescricao(despesa.getDescricao());
        despesaForm.setValor(despesa.getValor());
        despesaForm.setLocalizacao(despesa.getLocalizacao());
        despesaForm.setData(despesa.getData());
        despesaForm.setRecorrencia(despesa.getRecorrencia());

        model.put("categoriasMap", getcategoriaMap());
        model.put("despesaForm", despesaForm);

        return "form";

    }




    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public String delete_despesa(ModelMap model, @PathVariable("id") Long id) {


        Despesa despesa = em.find(Despesa.class, id);
        em.remove(despesa);

        model.addAttribute("message", "Sucesso! A despesa  foi apagado.");

        return "result";
    }





    @RequestMapping(value = "/mapaMensal", method = RequestMethod.GET)

    public String getMapaMensal(ModelMap model, Principal user) {


        if (user.getName() != null){
            utilizador = em.find(Utilizador.class, user.getName());
        }


        // obtem lista de despesas da BD
        List<Despesa> despesas = em.createQuery("select u from Despesa u where u.login = :parametroUser", Despesa.class ).
                setParameter("parametroUser", user.getName()).
                getResultList();






        Map categoriasMap = getcategoriaMap();
        Map<String,Double> valoresJan = new HashMap<>();
        Map<String,Double> valoresFev = new HashMap<>();
        Map<String,Integer> valoresFevariacao = new HashMap<>();
        Map<String,Double> valoresMarç = new HashMap<>();
        Map<String,Integer> valoresMarçvariacao = new HashMap<>();
        Map<String,Double> valoresAbril = new HashMap<>();
        Map<String,Integer> valoresAbrilvariacao = new HashMap<>();
        Map<String,Double> valoresMaio = new HashMap<>();
        Map<String,Integer> valoresMaiovariacao = new HashMap<>();
        Map<String,Double> valoresjunho = new HashMap<>();
        Map<String,Integer> valoresjunhovariacao = new HashMap<>();
        Map<String,Double> valoresJul= new HashMap<>();
        Map<String,Integer> valoresJulvariacao= new HashMap<>();
        Map<String,Double> valoresAgo = new HashMap<>();
        Map<String,Integer> valoresAgovariacao = new HashMap<>();
        Map<String,Double> valoresSet = new HashMap<>();
        Map<String,Integer> valoresSetvariacao = new HashMap<>();
        Map<String,Double> valoresOut = new HashMap<>();
        Map<String,Integer> valoresOutvariacao = new HashMap<>();
        Map<String,Double> valoresNov = new HashMap<>();
        Map<String,Integer> valoresNovvariacao = new HashMap<>();
        Map<String,Double> valoresDez = new HashMap<>();
        Map<String,Integer> valoresDezvariacao = new HashMap<>();
        Map<String,Double> TotaisCategorias = new HashMap<>();
        Map<String,Double> alertasCategorias = new HashMap<>();
//////blha para todos os totais mensais
        Double[] blha = {0D,0D,0D,0D,0D,0D,0D,0D,0D,0D,0D,0D};


        /////// blhe para
        Double[] blhtransporte = {0D,0D,0D,0D,0D,0D,0D,0D,0D,0D,0D,0D};
        Double[] blhalimentação = {0D,0D,0D,0D,0D,0D,0D,0D,0D,0D,0D,0D};
        Double[] blhpropinas = {0D,0D,0D,0D,0D,0D,0D,0D,0D,0D,0D,0D};
        Double[] blhrendas = {0D,0D,0D,0D,0D,0D,0D,0D,0D,0D,0D,0D};
        Double[] blhuniersidade = {0D,0D,0D,0D,0D,0D,0D,0D,0D,0D,0D,0D};
        Double[] blhindifinidas = {0D,0D,0D,0D,0D,0D,0D,0D,0D,0D,0D,0D};

        Integer[] LALA = {0,0,0,0,0,0,0,0,0,0,0,0};
        // ...

        for (Despesa despesa : despesas) {

            String categoriaName = despesa.getCategoria();



            // 1 - obter a data da despesa
            //2 - consoante a data usar a variavel valoresXXX correcta (ex:valoresFev)
            //3 - fazer put nessa variavel de despesaName,despesa.getvalor


            if(despesa.getData()==1){
                Double valorAnterior =  valoresJan.getOrDefault(categoriaName,0D);



                valoresJan.put(categoriaName,valorAnterior + despesa.getValor());



                Double v1 =   valoresJan.getOrDefault ("Transporte", 0D);
                Double v2 =   valoresJan.getOrDefault ("Propinas", 0D);
                Double v3 =   valoresJan.getOrDefault ("Rendas", 0D);
                Double v4 =   valoresJan.getOrDefault ("Universidade", 0D);
                Double v5 =   valoresJan.getOrDefault ("Alimentação", 0D);
                Double v6 =   valoresJan.getOrDefault ("Indifinidas",0D);
                Double  totaljan = 0D;

                totaljan=v1+v2+v3+v4+v5+v6;

                valoresJan.put("Total", totaljan);
                blha[0]=totaljan;

                blhtransporte[0]=v1;
                blhpropinas[0]=v2;
                blhrendas[0]=v3;
                blhuniersidade[0]=v4;
                blhalimentação[0]=v5;
                blhindifinidas[0]=v6;

            }



            if(despesa.getData()==2){
                Double valorAnterior =  valoresFev.getOrDefault(categoriaName,0D);

                valoresFev.put(categoriaName,valorAnterior + despesa.getValor());
                Double v1f =  valoresFev.getOrDefault ("Transporte", 0D);
                Double v2f =   valoresFev.getOrDefault ("Propinas", 0D);
                Double v3f =   valoresFev.getOrDefault ("Rendas", 0D);
                Double v4f =   valoresFev.getOrDefault ("Universidade", 0D);
                Double v5f =   valoresFev.getOrDefault ("Alimentação", 0D);
                Double v6f =   valoresFev.getOrDefault ("Indifinidas", 0D);
                Double  totalf = 0D;

                totalf=v1f+v2f+v3f+v4f+v5f+v6f;

                valoresFev.put("Total", totalf);

                blha[1]=totalf;
                blhtransporte[1]=v1f;
                blhpropinas[1]=v2f;
                blhrendas[1]=v3f;
                blhuniersidade[1]=v4f;
                blhalimentação[1]=v5f;
                blhindifinidas[1]=v6f;

            }



            if(despesa.getData()==3){
                Double valorAnterior =  valoresMarç.getOrDefault(categoriaName,0D);

                valoresMarç.put(categoriaName,valorAnterior + despesa.getValor());

                Double  v1m = valoresMarç.getOrDefault ("Transporte", 0D);
                Double  v2m = valoresMarç.getOrDefault ("Propinas",0D);
                Double v3m = valoresMarç.getOrDefault ("Rendas", 0D);
                Double  v4m = valoresMarç.getOrDefault ("Universidade", 0D);
                Double  v5m = valoresMarç.getOrDefault ("Alimentação", 0D);
                Double v6m = valoresMarç.getOrDefault ("Indifinidas", 0D);
                Double   totalm = 0D;

                totalm=v1m+v2m+v3m+v4m+v5m+v6m;

                valoresMarç.put("Total", totalm);

                blha[2]=totalm;
                blhtransporte[2]=v1m;
                blhpropinas[2]=v2m;
                blhrendas[2]=v3m;
                blhuniersidade[2]=v4m;
                blhalimentação[2]=v5m;
                blhindifinidas[2]=v6m;



            }

            if(despesa.getData()==4){
                Double valorAnterior =  valoresAbril.getOrDefault(categoriaName,0D);

                valoresAbril.put(categoriaName,valorAnterior + despesa.getValor());

                Double  v1a =  valoresAbril.getOrDefault ("Transporte", 0D);
                Double v2a =  valoresAbril.getOrDefault ("Propinas", 0D);
                Double  v3a =  valoresAbril.getOrDefault ("Rendas",0D);
                Double  v4a =  valoresAbril.getOrDefault ("Universidade", 0D);
                Double  v5a =  valoresAbril.getOrDefault ("Alimentação", 0D);
                Double  v6a =  valoresAbril.getOrDefault ("Indifinidas", 0D);
                Double   totala = 0D;

                totala=v1a+v2a+v3a+v4a+v5a+v6a;
                valoresAbril.put("Total",  totala);

                blha[3]=totala;
                blhtransporte[3]=v1a;
                blhpropinas[3]=v2a;
                blhrendas[3]=v3a;
                blhuniersidade[3]=v4a;
                blhalimentação[3]=v5a;
                blhindifinidas[3]=v6a;
            }
            if(despesa.getData()==5){
                Double  valorAnterior =  valoresMaio.getOrDefault(categoriaName, 0D);

                valoresMaio.put(categoriaName,valorAnterior + despesa.getValor());

                Double  v1ma =  valoresMaio.getOrDefault ("Transporte", 0D);
                Double  v2ma =  valoresMaio.getOrDefault ("Propinas", 0D);
                Double  v3ma =  valoresMaio.getOrDefault ("Rendas", 0D);
                Double  v4ma =  valoresMaio.getOrDefault ("Universidade", 0D);
                Double v5ma =  valoresMaio.getOrDefault ("Alimentação", 0D);
                Double v6ma =   valoresMaio.getOrDefault ("Indifinidas", 0D);
                Double  totalma = 0D;

                totalma=v1ma+v2ma+v3ma+v4ma+v5ma+v6ma;
                valoresMaio.put("Total",totalma);
                blha[4]=totalma;
                blhtransporte[4]=v1ma;
                blhpropinas[4]=v2ma;
                blhrendas[4]=v3ma;
                blhuniersidade[4]=v4ma;
                blhalimentação[4]=v5ma;
                blhindifinidas[4]=v6ma;
            }

            if(despesa.getData()==6){
                Double valorAnterior =  valoresjunho.getOrDefault(categoriaName, 0D);

                valoresjunho.put(categoriaName,valorAnterior + despesa.getValor());

                Double  v1jn =  valoresjunho.getOrDefault ("Transporte", 0D);
                Double  v2jn =  valoresjunho.getOrDefault ("Propinas", 0D);
                Double  v3jn =  valoresjunho.getOrDefault ("Rendas", 0D);
                Double v4jn =  valoresjunho.getOrDefault ("Universidade", 0D);
                Double  v5jn =  valoresjunho.getOrDefault ("Alimentação", 0D);
                Double v6jn =   valoresjunho.getOrDefault ("Indifinidas", 0D);
                Double   totaljn = 0D;

                totaljn=v1jn+v2jn+v3jn+v4jn+v5jn+v6jn;
                valoresjunho.put("Total", totaljn);

                blha[5]=totaljn;

                blhtransporte[5]=v1jn;
                blhpropinas[5]=v2jn;
                blhrendas[5]=v3jn;
                blhuniersidade[5]=v4jn;
                blhalimentação[5]=v5jn;
                blhindifinidas[5]=v6jn;
            }

            if(despesa.getData()==7){
                Double valorAnterior =  valoresJul.getOrDefault(categoriaName,0D);

                valoresJul.put(categoriaName,valorAnterior + despesa.getValor());


                Double  v1jl =   valoresJul.getOrDefault ("Transporte", 0D);
                Double  v2jl =   valoresJul.getOrDefault ("Propinas", 0D);
                Double v3jl =   valoresJul.getOrDefault ("Rendas", 0D);
                Double  v4jl =  valoresJul.getOrDefault ("Universidade", 0D);
                Double v5jl =   valoresJul.getOrDefault ("Alimentação", 0D);
                Double  v6jl =   valoresJul.getOrDefault ("Indifinidas", 0D);
                Double  totaljl = 0D;

                totaljl=v1jl+v2jl+v3jl+v4jl+v5jl+v6jl;
                valoresJul.put("Total", totaljl);

                blha[6]=totaljl;

                blhtransporte[6]=v1jl;
                blhpropinas[6]=v2jl;
                blhrendas[6]=v3jl;
                blhuniersidade[6]=v4jl;
                blhalimentação[6]=v5jl;
                blhindifinidas[6]=v6jl;
            }

            if(despesa.getData()==8){
                Double valorAnterior =  valoresAgo.getOrDefault(categoriaName,0D);

                valoresAgo.put(categoriaName,valorAnterior + despesa.getValor());

                Double v1ag =  valoresAgo.getOrDefault ("Transporte", 0D);
                Double v2ag =   valoresAgo.getOrDefault ("Propinas", 0D);
                Double v3ag =  valoresAgo.getOrDefault ("Rendas", 0D);
                Double v4ag = valoresAgo.getOrDefault ("Universidade", 0D);
                Double v5ag =   valoresAgo.getOrDefault ("Alimentação", 0D);
                Double v6ag =  valoresAgo.getOrDefault ("Indifinidas", 0D);
                Double totalag = 0D;

                totalag=v1ag+v2ag+v3ag+v4ag+v5ag+v6ag;
                valoresAgo.put("Total", totalag);

                blha[7]=totalag;

                blhtransporte[7]=v1ag;
                blhpropinas[7]=v2ag;
                blhrendas[7]=v3ag;
                blhuniersidade[7]=v4ag;
                blhalimentação[7]=v5ag;
                blhindifinidas[7]=v6ag;

            }

            if(despesa.getData()==9){
                Double valorAnterior =  valoresSet.getOrDefault(categoriaName,0D);

                valoresSet.put(categoriaName,valorAnterior + despesa.getValor());

                Double v1set = valoresSet.getOrDefault ("Transporte", 0D);
                Double v2set = valoresSet.getOrDefault ("Propinas", 0D);
                Double v3set =  valoresSet.getOrDefault ("Rendas", 0D);
                Double v4set = valoresSet.getOrDefault ("Universidade", 0D);
                Double v5set = valoresSet.getOrDefault ("Alimentação", 0D);
                Double v6set = valoresSet.getOrDefault ("Indifinidas", 0D);
                Double totalset = 0D;

                totalset=v1set+v2set+v3set+v4set+v5set+v6set;
                valoresSet.put("Total", totalset);

                blha[8]=totalset;
                blhtransporte[8]=v1set;
                blhpropinas[8]=v2set;
                blhrendas[8]=v3set;
                blhuniersidade[8]=v4set;
                blhalimentação[8]=v5set;
                blhindifinidas[8]=v6set;
            }
            if(despesa.getData()==10){
                Double valorAnterior =  valoresOut.getOrDefault(categoriaName, 0D);

                valoresOut.put(categoriaName,valorAnterior + despesa.getValor());

                Double v1out = valoresOut.getOrDefault ("Transporte", 0D);
                Double v2out =  valoresOut.getOrDefault ("Propinas", 0D);
                Double v3out =  valoresOut.getOrDefault ("Rendas", 0D);
                Double v4out =  valoresOut.getOrDefault ("Universidade", 0D);
                Double v5out =  valoresOut.getOrDefault ("Alimentação", 0D);
                Double v6out =  valoresOut.getOrDefault ("Indifinidas", 0D);
                Double totalout = 0D;

                totalout=v1out+v2out+v3out+v4out+v5out+v6out;
                valoresOut.put("Total", totalout);

                blha[9]=totalout;
                blhtransporte[9]=v1out;
                blhpropinas[9]=v2out;
                blhrendas[9]=v3out;
                blhuniersidade[9]=v4out;
                blhalimentação[9]=v5out;
                blhindifinidas[9]=v6out;
            }

            if(despesa.getData()==11){

                Double valorAnterior =  valoresNov.getOrDefault(categoriaName, 0D);


                valoresNov.put(categoriaName,valorAnterior + despesa.getValor());

                Double v1nov = valoresNov.getOrDefault ("Transporte", 0D);
                Double v2nov = valoresNov.getOrDefault ("Propinas", 0D);
                Double v3nov = valoresNov.getOrDefault ("Rendas", 0D);
                Double v4nov =   valoresNov.getOrDefault ("Universidade", 0D);
                Double v5nov =   valoresNov.getOrDefault ("Alimentação", 0D);
                Double v6nov =   valoresNov.getOrDefault ("Indifinidas", 0D);
                Double totalnov = 0D;

                totalnov=v1nov+v2nov+v3nov+v4nov+v5nov+v6nov;

                valoresNov.put("Total",totalnov);

                blha[10]=totalnov;

                blhtransporte[10]=v1nov;
                blhpropinas[10]=v2nov;
                blhrendas[10]=v3nov;
                blhuniersidade[10]=v4nov;
                blhalimentação[10]=v5nov;
                blhindifinidas[10]=v6nov;


            }

            if(despesa.getData()==12){
                Double valorAnterior =  valoresDez.getOrDefault(categoriaName,0D);

                valoresDez.put(categoriaName,valorAnterior + despesa.getValor());


                Double v1dez =  valoresDez.getOrDefault ("Transporte", 0D);
                Double v2dez =  valoresDez.getOrDefault ("Propinas", 0D);
                Double v3dez =  valoresDez.getOrDefault ("Rendas", 0D);
                Double  v4dez =  valoresDez.getOrDefault ("Universidade", 0D);
                Double v5dez =  valoresDez.getOrDefault ("Alimentação", 0D);
                Double v6dez =  valoresDez.getOrDefault ("Indifinidas", 0D);
                Double  totaldez = 0D;

                totaldez=v1dez+v2dez+v3dez+v4dez+v5dez+v6dez;
                valoresDez.put("Total", totaldez);

                blha[11]=totaldez;
                blhtransporte[11]=v1dez;
                blhpropinas[5]=v2dez;
                blhrendas[5]=v3dez;
                blhuniersidade[5]=v4dez;
                blhalimentação[5]=v5dez;
                blhindifinidas[5]=v6dez;

            }
            else{
            }

            if( blha[1]>0 && blha[0]>0){
                Double Ef= blha[1]- blha[0];
                Double Cf=(Ef/blha[0]);
                Double wf= (Cf*100);
                Integer If = wf.intValue();
                valoresFevariacao.put("variação",If);
                LALA[0]=If;
            }
            else{
                valoresFevariacao.put("variação",0);
            }

            if( blha[1]>0 && blha[2]>0){
                Double EmÇ= blha[2]- blha[1];
                Double Cmç=(EmÇ/blha[1]);
                Double wmç= (Cmç*100);
                Integer ImÇ= wmç.intValue();
                valoresMarçvariacao.put("variação",ImÇ);

                LALA[1]=ImÇ;
            }

            else{

                valoresMarçvariacao.put("variação",0);
            }

            if( blha[2]>0 && blha[3]>0){
                Double Eab= blha[3]- blha[2];
                Double Cab=(Eab/blha[2]);
                Double wab= (Cab*100);
                Integer Iab= wab.intValue();
                valoresAbrilvariacao.put("variação",Iab);
                LALA[2]=Iab;
            }
            else{
                valoresAbrilvariacao.put("variação",0);
            }
            if( blha[3]>0 && blha[4]>0){
                Double Emi= blha[4]- blha[3];
                Double Cmi=(Emi/blha[3]);
                Double wmi= (Cmi*100);
                Integer Imi= wmi.intValue();
                valoresMaiovariacao.put("variação",Imi);
                LALA[3]=Imi;

            }
            else{
                valoresMaiovariacao.put("variação",0);
            }

            if( blha[4]>0 && blha[5]>0){
                Double Ejn= blha[5]- blha[4];
                Double Cjn=(Ejn/blha[4]);
                Double wjn= (Cjn*100);
                Integer Ijn= wjn.intValue();
                valoresjunhovariacao.put("variação",Ijn);
                LALA[4]=Ijn;
            }
            else{
                valoresjunhovariacao.put("variação",0);
            }

            if( blha[5]>0 && blha[6]>0){
                Double Ejl= blha[6]- blha[5];
                Double Cjl=(Ejl/blha[5]);
                Double wjl= (Cjl*100);
                Integer Ijl= wjl.intValue();
                valoresJulvariacao.put("variação",Ijl);
                LALA[5]=Ijl;

            }
            else{
                valoresJulvariacao.put("variação",0);
            }

            if( blha[6]>0 && blha[7]>0){
                Double Eag= blha[7]- blha[6];
                Double Cag=(Eag/blha[6]);
                Double wag= (Cag*100);
                Integer Iag= wag.intValue();
                valoresAgovariacao.put("variação",Iag);
                LALA[6]=Iag;
            }
            else{
                valoresAgovariacao.put("variação",0);
            }

            if( blha[7]>0 && blha[8]>0){
                Double Eset= blha[8]- blha[7];
                Double Cset=(Eset/blha[7]);
                Double wset= (Cset*100);
                Integer Iset= wset.intValue();
                valoresSetvariacao.put("variação",Iset);
                LALA[7]=Iset;
            }
            else{
                valoresSetvariacao.put("variação",0);
            }

            if( blha[8]>0&& blha[9]>0){
                Double Eout= blha[9]- blha[8];
                Double Cout=(Eout/blha[8]);
                Double wout= (Cout*100);
                Integer Iout= wout.intValue();
                valoresOutvariacao.put("variação",Iout);
                LALA[8]=Iout;
            }
            else{
                valoresOutvariacao.put("variação",0);
            }

            if( blha[9]>0&& blha[10]>0){
                Double Enove= blha[10]- blha[9];
                Double Cnove=(Enove/blha[9]);
                Double wnove= (Cnove*100);
                Integer Inove= wnove.intValue();
                valoresNovvariacao.put("variação",Inove);
                LALA[9]=Inove;
            }
            else{
                valoresNovvariacao.put("variação",0);
            }


            if( blha[10]>0 && blha[11]>0){
                Double Edez= blha[11]- blha[10];
                Double Cdez=(Edez/blha[10]);
                Double wdez= (Cdez*100);
                Integer Idez= wdez.intValue();
                valoresDezvariacao.put("variação",Idez);
                LALA[10]=Idez;
            }

            else{
                valoresDezvariacao.put("variação",0);
            }

        }



        Double totalAlimentação=  blhalimentação[0]+blhalimentação[1]+ blhalimentação[2]+ blhalimentação[3]+ blhalimentação[4]+ blhalimentação[5]+ blhalimentação[6]+ blhalimentação[7]+ blhalimentação[8]+ blhalimentação[9]+ blhalimentação[10]+ blhalimentação[11];
        Double totalTransporte =blhtransporte[0]+ blhtransporte[1]+blhtransporte[2]+blhtransporte[3]+ blhtransporte[4]+blhtransporte[5]+blhtransporte[6]+blhtransporte[7]+blhtransporte[8]+blhtransporte[9]+blhtransporte[10]+ blhtransporte[11];
        Double totalRendas= blhrendas[0]+blhrendas[1]+blhrendas[2]+blhrendas[3]+blhrendas[4]+blhrendas[5]+blhrendas[6]+blhrendas[7]+blhrendas[8]+blhrendas[9]+blhrendas[10]+blhrendas[11];
        Double totaluniversidade=blhuniersidade[0]+blhuniersidade[1]+blhuniersidade[2]+blhuniersidade[3]+blhuniersidade[4]+blhuniersidade[5]+blhuniersidade[6]+blhuniersidade[7]+blhuniersidade[8]+blhuniersidade[9]+blhuniersidade[10]+blhuniersidade[11];
        Double totalPropinas=blhpropinas[0]+blhpropinas[1]+blhpropinas[2]+blhpropinas[3]+blhpropinas[4]+blhpropinas[5]+blhpropinas[6]+blhpropinas[7]+blhpropinas[8]+blhpropinas[9]+blhpropinas[10]+blhpropinas[11];
        Double totalIdifinidas=blhindifinidas[0]+blhindifinidas[1]+blhindifinidas[2]+blhindifinidas[3]+blhindifinidas[4]+blhindifinidas[5]+blhindifinidas[6]+blhindifinidas[7]+blhindifinidas[8]+blhindifinidas[9]+blhindifinidas[10]+blhindifinidas[11];
        Integer totalVariação= LALA[0]+LALA[1]+LALA[2]+LALA[3]+LALA[4]+LALA[5]+LALA[6]+LALA[7]+LALA[8]+LALA[9]+LALA[10];

        Double totaistais= blha[0]+blha[1]+blha[2]+blha[3]+blha[4]+blha[5]+blha[6]+blha[7]+blha[8]+blha[9]+blha[10]+blha[11];

        TotaisCategorias.put("totalAlimentação",totalAlimentação);
        TotaisCategorias.put("totalTransporte",totalTransporte );
        TotaisCategorias.put("totalRendas",totalRendas);
        TotaisCategorias.put("totaluniversidade",totaluniversidade);
        TotaisCategorias.put("totalPropinas",totalPropinas);
        TotaisCategorias.put("totaistais",totaistais);
        TotaisCategorias.put("totalIdifinidas",totalIdifinidas);
        valoresDezvariacao.put("totalVariação", totalVariação);

        Double alertaTransporte=60.45;
        Double alertaTransporteaviso=55.45;

        Double alertaAlimentacao=78.45;
        Double alertaAlimentacaoeaviso=60.45;

        Double alertaPropinas=450D;
        Double alertaPropinasaviso=350D;

        Double alertaRendas=400D;
        Double alertaRendassaviso=350D;

        Double alertatotais=900D;
        Double alertatotaisssaviso=750D;

        alertasCategorias.put("alertaTransporte",alertaTransporte);
        alertasCategorias.put("alertaTransporteaviso",alertaTransporteaviso);

        alertasCategorias.put("alertaAlimentacao", alertaAlimentacao);
        alertasCategorias.put("alertaAlimentacaoeaviso",alertaTransporteaviso);

        alertasCategorias.put("alertaPropinas",alertaPropinas);
        alertasCategorias.put("alertaPropinasaviso",alertaPropinasaviso);

        alertasCategorias.put("alertaRendas",alertaRendas);
        alertasCategorias.put("alertaRendassaviso",alertaRendassaviso);

        alertasCategorias.put("alertatotais",alertatotais);
        alertasCategorias.put("alertatotaisssaviso",alertatotaisssaviso);



        model.put("alertasCategorias",alertasCategorias);
        model.put("TotaisCategorias",TotaisCategorias);
        model.put("valoresFevariacao", valoresFevariacao);

        model.put("valoresJan",valoresJan);
        model.put("Total",valoresJan);
        model.put("valoresFev",valoresFev);
        model.put("valoresMarç",valoresMarç);
        model.put("valoresMarçvariacao",valoresMarçvariacao);
        model.put("valoresAbril",valoresAbril);
        model.put("valoresAbrilvariacao",valoresAbrilvariacao);
        model.put("valoresMaio",valoresMaio);
        model.put("valoresMaiovariacao",valoresMaiovariacao);
        model.put("valoresjunho",valoresjunho);
        model.put("valoresjunhovariacao",valoresjunhovariacao);
        model.put("valoresJul",valoresJul);
        model.put("valoresJulvariacao",valoresJulvariacao);
        model.put("valoresAgo",valoresAgo);
        model.put("valoresAgovariacao",valoresAgovariacao);
        model.put("valoresSet",valoresSet);
        model.put("valoresSetvariacao",valoresSetvariacao);
        model.put("valoresOut",valoresOut);
        model.put("valoresOutvariacao",valoresOutvariacao);
        model.put("valoresNov",valoresNov);
        model.put("valoresNovvariacao",valoresNovvariacao);
        model.put("valoresDez",valoresDez);
        model.put("valoresDezvariacao",valoresDezvariacao);

        return "mapaMensal";
    }



    @RequestMapping(value = "/criarcategoria", method = RequestMethod.GET)
    public String getcategoriaForm(ModelMap model, Principal user) {
        model.put("categoriaForm", new CategoriaForm());

        if(user.getName().equals("admin")){
            model.addAttribute("permissao", "admin");
        }


        return "criarcategoria";
    }



    @RequestMapping(value = "/criarcategoria", method = RequestMethod.POST)
    public String submitcategoriaForm(@Valid @ModelAttribute("CategoriaForm") CategoriaForm categoriaForm,
                                      BindingResult bindingResult,
                                      ModelMap model,Principal user) {

        if (bindingResult.hasErrors()) {
            return "criarcategoria";
        }


        Categoria categoria;
        if(categoriaForm.getId()!= null){
            categoria = em.find(Categoria.class,categoriaForm.getId()); // se é um update
        }

        else{
            categoria = new Categoria();
        }

        categoria.setName(categoriaForm.getName());
        em.persist(categoria);

        if(categoriaForm.getId() != null) {
            model.addAttribute("message", "Sucesso! A categoria foi editada");
        }
        else {
            model.addAttribute("message", "Sucesso! A categoria  foi criada");
        }



        model.addAttribute("message", "Sucesso! A Categoria " + categoria.getName());

        return "result";
    }


    @RequestMapping(value = "/listcategorias", method = RequestMethod.GET)
    public String getListcategorias(ModelMap model, Principal user) {

        List<Categoria> categorias = em.createQuery("select e from  Categoria e", Categoria.class).getResultList();

        for(Categoria categoria : categorias) {
            categoria.setName(categoria.getName());
            categoria.setId(categoria.getId());

        }


        model.put("categorias",categorias);
        return "listcategorias";
    }





    @RequestMapping(value = "/deletecategoria/{id}", method = RequestMethod.POST)
    public String delete_categoria(ModelMap model, @PathVariable("id") Integer id) {

        Categoria categoria = em.find(Categoria.class, id);

        if (categoria.getId()==1 ||categoria.getId()==2 ||categoria.getId()==3 ||categoria.getId()==4 ||categoria.getId()==5){

            model.addAttribute("message", "Erro  a categoria não pode ser apagada eviva ao FC Porto.");
        }


        else{
            em.remove(categoria);

            model.addAttribute("message", "Sucesso! A Categoria foi apagado.");
        }
        return "result";
    }


    @RequestMapping(value = "/editcategoria/{id}", method = RequestMethod.GET)
    public String editcategoria_categoria(ModelMap model, @PathVariable("id") Integer id, Principal user){


        Categoria categoria = em.find(Categoria.class, id);

        if (categoria.getId()==1 ||categoria.getId()==2 ||categoria.getId()==3 ||categoria.getId()==4 ||categoria.getId()==5){

            model.addAttribute("message", "Erro  a categoria não pode ser editada  e viva ao FC Porto.");
            return "result";
        }
        else {
            CategoriaForm categoriaForm = new CategoriaForm();
            categoriaForm.setId(categoria.getId());
            categoriaForm.setName(categoria.getName());
            model.put("categoriaForm",categoriaForm );
        }




        return "criarcategoria";

    }



    @RequestMapping(value = "/listutlizadores", method = RequestMethod.GET)
    public String getListutilizadores(ModelMap model, Principal user) {

        List<Utilizador> utilizadores = em.createQuery("select e from  Utilizador e", Utilizador.class).getResultList();

        for(Utilizador utilizador : utilizadores) {
            utilizador.setNome(utilizador.getNome());
           utilizador.setAgregado(utilizador.getAgregado());
           utilizador.setEmail(utilizador.getEmail());

        }


        model.put("utilizadores", utilizadores);
        return "listutlizadores";
    }


    @RequestMapping(value ="/perfil", method = RequestMethod.GET)
    public String getperfilblhaForm(ModelMap model, Principal user) {


        Utilizador utilizador = em.find(Utilizador.class,user.getName());
        model.put("utilizador",utilizador);

        return "perfil";
    }




    @RequestMapping(value = "/criarperfil", method = RequestMethod.GET)
    public String getcriarperfilForm(ModelMap model, Principal user) {

        UtilizadorForm utilizadorForm =new UtilizadorForm();

        Utilizador utilizador = em.find(Utilizador.class, user.getName());

        if(utilizador!= null){
            utilizadorForm.setNome(utilizador.getNome());
            utilizadorForm.setAgregado(utilizador.getAgregado());
            utilizadorForm.setEmail(utilizador.getEmail());
        }

    model.put("utilizadorForm",utilizadorForm);
        return "criarperfil";
    }


    @RequestMapping(value = "/criarperfil", method = RequestMethod.POST)
    public String submitperfilForm(@Valid @ModelAttribute("UtilizadorForm") UtilizadorForm utilizadorForm,
                                      BindingResult bindingResult,
                                      ModelMap model,Principal user) {

        if (bindingResult.hasErrors()) {
            return "criarperfil";
        }



        if(user.getName()!= null){
            utilizador = em.find(Utilizador.class,user.getName());
        }


        if(utilizador== null){
            utilizador =new Utilizador();

        }


        utilizador.setLogin(user.getName());
        utilizador.setNome(utilizadorForm.getNome());
        utilizador.setEmail(utilizadorForm.getEmail());
        utilizador.setAgregado(utilizadorForm.getAgregado());
        em.persist(utilizador);

        return "result";
    }






    @RequestMapping(value = "/adicionaragregado/{login}", method = RequestMethod.POST)
    public String delete_categoria(ModelMap model, @PathVariable("login") String login) {

       Utilizador utilizador= em.find(Utilizador.class, login);




        em.persist((utilizador.getLogin()));
        return "result";
    }










}
