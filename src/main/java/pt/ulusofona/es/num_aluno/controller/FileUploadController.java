package pt.ulusofona.es.num_aluno.controller;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import pt.ulusofona.es.num_aluno.data.Despesa;
import pt.ulusofona.es.num_aluno.form.UploadForm;
import pt.ulusofona.es.num_aluno.form.UtilizadorForm;
import pt.ulusofona.es.num_aluno.data.Utilizador;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;
import java.io.*;
import java.security.Principal;

@Controller
@Transactional
public class FileUploadController {

    @PersistenceContext
    private EntityManager em;
    public static final String UPLOAD_FOLDER = "upload";
    // para ter a certeza que existe a pasta upload
    static {
        new File(UPLOAD_FOLDER).mkdir();
    }

    @RequestMapping(value = "/upload", method = RequestMethod.GET)
    public String getUploadForm(ModelMap model) {
        model.put("uploadForm", new UploadForm());
        return "uploadForm";
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String handleFileUpload(@Valid @ModelAttribute("uploadForm") UploadForm uploadForm,
                                   BindingResult bindingResult,
                                   @RequestParam("file") MultipartFile file,
                                   ModelMap model, Principal user) {

        if (bindingResult.hasErrors()) {
            return "uploadForm";
        }

        if (!file.isEmpty()) {


            try {
                String fileName = UPLOAD_FOLDER + "/" + System.currentTimeMillis() + "-" + file.getOriginalFilename();
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fileName));
                stream.write(file.getBytes());
                stream.close();

                String  linha="";
                String divisor=",";
                BufferedReader leitura=null;

                leitura = new BufferedReader(new FileReader(fileName));
                int i=0;



                while ((linha= leitura.readLine()) !=null){
                    if (i!=0){
                    String []percorrer =linha.split(divisor);

                    percorrer[1].replace(",","");
                    Double valor = new Double(percorrer[1]);
                    if (percorrer.length>0) {
                        Despesa despesa= new Despesa();
                        long ctg=100;
                        long datames =0;
                        String Categorianome="Indifinidas";

                        if(percorrer[0].equals("Janeiro")){
                            datames =1;
                        }
                        else if(percorrer[0].equals("Fevereiro")){
                            datames =2;
                        }
                        else if(percorrer[0].equals("Março")){
                            datames =3;
                        }
                        else if(percorrer[0].equals("Abril")){
                            datames =4;
                        }
                        else if(percorrer[0].equals("Maio")){
                            datames =5;
                        }
                        else if(percorrer[0].equals("Junho")){
                            datames =6;
                        }
                        else if(percorrer[0].equals("Julho")){
                            datames =7;
                        }

                        else if(percorrer[0].equals("Agosto")){
                            datames =8;
                        }
                        else if(percorrer[0].equals("Setembro")){
                            datames =9;
                        }

                        else if(percorrer[0].equals("Outubro")){
                            datames =10;
                        }

                        else if(percorrer[0].equals("Novembro")){
                            datames =11;
                        }

                        else if(percorrer[0].equals("Dezembro")){
                            datames =12;
                        }

                        else{
                            datames =0;
                        }
                        despesa.setData(datames);
                        despesa.setValor(valor);
                        despesa.setCategoria(Categorianome);
                        despesa.setCategoriaName(Categorianome);
                        despesa.setLocalizacao(percorrer[2]);
                        despesa.setDescricao(percorrer[3]);
                        despesa.setRecorrencia(percorrer[4]);
                        despesa.setLogin(user.getName());
                        em.persist(despesa);

                    }
                }
                    else{
                      i++;
                    }

                }

                model.put("message", "Sucesso. O ficheiro " + file.getOriginalFilename() + " foi guardado com sucesso!");
                return "uploadResult";

            } catch (Exception e) {
                model.put("message", "Falha no upload => " + e.getMessage());
                return "uploadResult";
            }
        } else {
            model.put("message", "Ficheiro vazio");
            return "uploadResult";
        }

    }
}