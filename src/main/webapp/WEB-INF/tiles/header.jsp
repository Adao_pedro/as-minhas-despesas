<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<div class="header">
<security:authorize access="isAuthenticated()">
    <p>Hello <security:authentication property="principal.username"/>, <a href="/logout">Logout</a></p>
</security:authorize>

<security:authorize access="!isAuthenticated()">
<p>Not logged in</p>
</security:authorize>

<a href="/list">Listar</a> | <a href="/form">Nova</a>  | <a href="/mapaMensal">Mapa Mensal</a> |<security:authorize access="hasRole('ROLE_ADMIN')"><a href="/criarcategoria">Criar categoria</a></security:authorize>
  |<security:authorize access="hasRole('ROLE_ADMIN')"><a href="/listcategorias">Listar Categoria</a></security:authorize>| <a href="/listutlizadores">Utilizadores</a> <a href="/perfil">Perfil</a>
</div>