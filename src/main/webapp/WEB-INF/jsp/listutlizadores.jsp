<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">

        <c:choose>
            <c:when test="${not empty utilizadores}">
                <table border="1" align="center">
                    <tr>
                        <th>Nome</th>
                        <th>Agregado</th>
                        <th>email</th>
                        <th>Adicionar ao Agregado</th>
                    </tr>
                    <c:forEach var="utilizador" items="${utilizadores}">
                        <tr>
                            <td>${utilizador.nome }</td>
                            <td>${utilizador.agregado}</td>
                            <td>${utilizador.email}</td>
                           <td> <form action="/adicionaragregado/${login}" method="post">
                                <input type="submit" value="submit" />
                            </form></td>
                        </tr>
                    </c:forEach>
                </table>
            </c:when>
            <c:otherwise>
                <p>Não tem resultados</p>
            </c:otherwise>
        </c:choose>
        <p align="center">
            <a href="/form">Inserir uma nova despesa </a>
        </p>

    </tiles:putAttribute>
</tiles:insertDefinition>
