<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">

        <form:form method="post" modelAttribute="categoriaForm" action="/criarcategoria">
            <form:hidden path="id"/>

            <form:label path="name">descricao</form:label>
            <form:input path="name" label="name" />

            <form:errors path="name" cssClass="name"/><br/>

            <input type="submit" name="Gravar"/>
        </form:form>

    </tiles:putAttribute>
</tiles:insertDefinition>
