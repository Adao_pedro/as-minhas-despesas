<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">

        <c:choose>
            <c:when test="${not empty categorias}">
                <table border="1" align="center">
                    <tr>
                        <th>Nome</th>
                        <th>Editar</th>
                        <th>Apagar</th>
                    </tr>

                    <tr>

                    </tr>
                    <c:forEach var="categoria" items="${categorias}">
                        <tr>
                            <td>${categoria.name }</td>

                            <td><a href="/editcategoria/${categoria.id}">Editar</a></td>
                            <td><form action="/deletecategoria/${categoria.id}" method="post">
                                <input type="submit" value="Apagar" />
                            </form></td>
                        </tr>
                    </c:forEach>


                </table>
            </c:when>
            <c:otherwise>
                <p>Não tem resultados</p>
            </c:otherwise>
        </c:choose>
        <p align="center">
            <a href="/criarcategoria">Inserir uma nova categoria</a>
        </p>

    </tiles:putAttribute>
</tiles:insertDefinition>
