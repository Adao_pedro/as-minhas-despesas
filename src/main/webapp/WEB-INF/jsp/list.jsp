<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">

        <c:choose>
            <c:when test="${not empty despesas}">
                <table border="1" align="center">
                    <tr>
                        <th>Data</th>
                        <th>Categoria</th>
                        <th>valor</th>
                        <th>Mais </th>
                    </tr>
                    <c:forEach var="despesa" items="${despesas}">
                        <tr>
                            <td>${despesa.dataName }</td>
                            <td>${despesa.categoriaName }</td>
                            <td>${despesa.valor}</td>
                            <td><a href="/info/${despesa.id}">Mais info</a></td>
                        </tr>
                    </c:forEach>
                </table>
            </c:when>
            <c:otherwise>
                <p>Não tem resultados</p>
            </c:otherwise>
        </c:choose>
        <p align="center">
            <a href="/form">Inserir uma nova despesa </a>
        </p>

    </tiles:putAttribute>
</tiles:insertDefinition>
