<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">

        <form:form method="post" modelAttribute="utilizadorForm" action="/criarperfil">

            <form:hidden path="login"/>

            <form:label path="nome">Nome</form:label>
            <form:input path="nome" label="nome" />

            <form:errors path="nome" cssClass="nome"/><br/>


            <form:label path="email">Email</form:label>
            <form:input path="email" label="email" />

            <form:errors path="email" cssClass="email"/><br/>

            <form:label path="agregado">Agregado</form:label>
            <form:input path="agregado" label="agregado" />

            <form:errors path="agregado" cssClass="agregado"/><br/>


            <input type="submit" name="Gravar"/>
        </form:form>

    </tiles:putAttribute>
</tiles:insertDefinition>
