<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">



        <table border="1" align="center" class="table table-inverse">
            <tr>
                <th >Mes</th>
                <th>Transporte</th>
                <th>Propinas</th>
                <th>Alimentação</th>
                <th>Rendas</th>
                <th>Indifinidas</th>
                <th>Total</th>
                <th>Variação</th>
            </tr>

            <tr>

                <td>Janeiro</td>
                <td bgcolor="${valoresJan.get("Transporte")>=alertasCategorias.get("alertaTransporte")? "red":"" || valoresJan.get("Transporte")>alertasCategorias.get("alertaTransporteaviso")? "orange":"" }" >${valoresJan.get("Transporte")}</td>
                <td bgcolor="${valoresJan.get("Propinas")>=alertasCategorias.get("alertaPropinas")? "red":"" || valoresJan.get("Propinas")>alertasCategorias.get("alertaPropinasaviso")? "orange":"" }"  >${valoresJan.get("Propinas")}</td>
                <td bgcolor="${valoresJan.get("Alimentação")>=alertasCategorias.get("alertaAlimentacao")? "red":"" || valoresJan.get("Alimentação")>alertasCategorias.get("alertaAlimentacaoeaviso")? "orange":"" }" >${valoresJan.get("Alimentação")}</td>
                <td bgcolor="${valoresJan.get("Rendas")>=alertasCategorias.get("alertaRendas")? "red":"" || valoresJan.get("Rendas")>alertasCategorias.get("alertaRendassaviso")? "orange":"" }" >${valoresJan.get("Rendas")}</td>
                <td>${valoresJan.get("Indifinidas")}</td>
                <td bgcolor="${valoresJan.get("Total")>=alertasCategorias.get("alertatotais")? "red":"" || valoresJan.get("Total")>alertasCategorias.get("alertatotaisssaviso")? "orange":"" }" >${valoresJan.get("Total")}</td>
                <td>${valoresJan.get("Variação")}</td>

            </tr>


            <tr>
                <td>Fevereiro</td>
                <td bgcolor="${valoresFev.get("Transporte")>=alertasCategorias.get("alertaTransporte")? "red":"" || valoresFev.get("Transporte")>alertasCategorias.get("alertaTransporteaviso")? "orange":"" }" >${ valoresFev.get("Transporte")}</td>
                <td bgcolor="${valoresFev.get("Propinas")>=alertasCategorias.get("alertaPropinas")? "red":"" || valoresFev.get("Propinas")>alertasCategorias.get("alertaPropinasaviso")? "orange":"" }">${ valoresFev.get("Propinas")}</td>
                <td bgcolor="${valoresFev.get("Alimentação")>=alertasCategorias.get("alertaAlimentacao")? "red":"" ||valoresFev.get("Alimentação")>alertasCategorias.get("alertaAlimentacaoeaviso")? "orange":"" }">${ valoresFev.get("Alimentação")}</td>
                <td bgcolor="${valoresFev.get("Rendas")>=alertasCategorias.get("alertaRendas")? "red":"" || valoresFev.get("Rendas")>alertasCategorias.get("alertaRendassaviso")? "orange":"" }" >${ valoresFev.get("Rendas")}</td>
                <td>${valoresFev.get("Indifinidas")}</td>
                <td bgcolor="${valoresFev.get("Total")>=alertasCategorias.get("alertatotais")? "red":"" || valoresFev.get("Total")>alertasCategorias.get("alertatotaisssaviso")? "orange":"" }">${valoresFev.get("Total")}</td>
                <td>${valoresFevariacao.get("variação")}</td>

            </tr>

            <tr>
                <td>Março</td>
                <td bgcolor="${valoresMarç.get("Transporte")>=alertasCategorias.get("alertaTransporte")? "red":"" || valoresMarç.get("Transporte")>alertasCategorias.get("alertaTransporteaviso")? "orange":"" }">${ valoresMarç.get("Transporte")}</td>
                <td bgcolor="${valoresMarç.get("Propinas")>=alertasCategorias.get("alertaPropinas")? "red":"" || valoresMarç.get("Propinas")>alertasCategorias.get("alertaPropinasaviso")? "orange":"" }">${ valoresMarç.get("Propinas")}</td>
                <td bgcolor="${valoresMarç.get("Alimentação")>=alertasCategorias.get("alertaAlimentacao")? "red":"" ||valoresMarç.get("Alimentação")>alertasCategorias.get("alertaAlimentacaoeaviso")? "orange":"" }">${ valoresMarç.get("Alimentação")}</td>
                <td bgcolor="${valoresMarç.get("Rendas")>=alertasCategorias.get("alertaRendas")? "red":"" || valoresMarç.get("Rendas")>alertasCategorias.get("alertaRendassaviso")? "orange":"" }">${ valoresMarç.get("Rendas")}</td>
                <td>${valoresMarç.get("Indifinidas")}</td>
                <td bgcolor="${valoresMarç.get("Total")>=alertasCategorias.get("alertatotais")? "red":"" || valoresMarç.get("Total")>alertasCategorias.get("alertatotaisssaviso")? "orange":"" }">${ valoresMarç.get("Total")}</td>
                <td>${valoresMarçvariacao.get("variação")}</td>


            </tr>


            <tr>
                <td>Abril</td>
                <td bgcolor="${valoresAbril.get("Transporte")>=alertasCategorias.get("alertaTransporte")? "red":"" || valoresAbril.get("Transporte")>alertasCategorias.get("alertaTransporteaviso")? "orange":"" }">${ valoresAbril.get("Transporte")}</td>
                <td bgcolor="${valoresAbril.get("Propinas")>=alertasCategorias.get("alertaPropinas")? "red":"" || valoresAbril.get("Propinas")>alertasCategorias.get("alertaPropinasaviso")? "orange":"" }">${ valoresAbril.get("Propinas")}</td>
                <td bgcolor="${valoresAbril.get("Alimentação")>=alertasCategorias.get("alertaAlimentacao")? "red":"" ||valoresAbril.get("Alimentação")>alertasCategorias.get("alertaAlimentacaoeaviso")? "orange":"" }">${ valoresAbril.get("Alimentação")}</td>
                <td bgcolor="${valoresAbril.get("Rendas")>=alertasCategorias.get("alertaRendas")? "red":"" || valoresAbril.get("Rendas")>alertasCategorias.get("alertaRendassaviso")? "orange":"" }">${ valoresAbril.get("Rendas")}</td>
                <td>${valoresAbril.get("Indifinidas")}</td>
                <td bgcolor="${valoresAbril.get("Total")>=alertasCategorias.get("alertatotais")? "red":"" || valoresAbril.get("Total")>alertasCategorias.get("alertatotaisssaviso")? "orange":"" }">${ valoresAbril.get("Total")}</td>
                <td>${ valoresAbrilvariacao.get("variação")}</td>

            </tr>


            <tr>
                <td>Maio</td>
                <td bgcolor="${valoresMaio.get("Transporte")>=alertasCategorias.get("alertaTransporte")? "red":"" || valoresMaio.get("Transporte")>alertasCategorias.get("alertaTransporteaviso")? "orange":"" }">${ valoresMaio.get("Transporte")}</td>
                <td bgcolor="${valoresMaio.get("Propinas")>=alertasCategorias.get("alertaPropinas")? "red":"" || valoresMaio.get("Propinas")>alertasCategorias.get("alertaPropinasaviso")? "orange":"" }">${ valoresMaio.get("Propinas")}</td>
                <td bgcolor="${valoresMaio.get("Alimentação")>=alertasCategorias.get("alertaAlimentacao")? "red":"" ||valoresMaio.get("Alimentação")>alertasCategorias.get("alertaAlimentacaoeaviso")? "orange":"" }">${ valoresMaio.get("Alimentação")}</td>
                <td bgcolor="${valoresMaio.get("Rendas")>=alertasCategorias.get("alertaRendas")? "red":"" || valoresMaio.get("Rendas")>alertasCategorias.get("alertaRendassaviso")? "orange":"" }">${ valoresMaio.get("Rendas")}</td>
                <td>${valoresMaio.get("Indifinidas")}</td>
                <td bgcolor="${valoresMaio.get("Total")>=alertasCategorias.get("alertatotais")? "red":"" || valoresMaio.get("Total")>alertasCategorias.get("alertatotaisssaviso")? "orange":"" }">${ valoresMaio.get("Total")}</td>
                <td>${ valoresMaiovariacao.get("variação")}</td>
            </tr>


            <tr>
                <td>Junho</td>
                <td bgcolor="${valoresjunho.get("Transporte")>=alertasCategorias.get("alertaTransporte")? "red":"" || valoresjunho.get("Transporte")>alertasCategorias.get("alertaTransporteaviso")? "orange":"" }">${ valoresjunho.get("Transporte")}</td>
                <td bgcolor="${valoresjunho.get("Propinas")>=alertasCategorias.get("alertaPropinas")? "red":"" || valoresjunho.get("Propinas")>alertasCategorias.get("alertaPropinasaviso")? "orange":"" }">${valoresjunho.get("Propinas")}</td>
                <td bgcolor="${valoresjunho.get("Alimentação")>=alertasCategorias.get("alertaAlimentacao")? "red":"" ||valoresjunho.get("Alimentação")>alertasCategorias.get("alertaAlimentacaoeaviso")? "orange":"" }">${ valoresjunho.get("Alimentação")}</td>
                <td bgcolor="${valoresjunho.get("Rendas")>=alertasCategorias.get("alertaRendas")? "red":"" ||valoresjunho.get("Rendas")>alertasCategorias.get("alertaRendassaviso")? "orange":"" }">${ valoresjunho.get("Rendas")}</td>
                <td>${valoresjunho.get("Indifinidas")}</td>
                <td bgcolor="${valoresjunho.get("Total")>=alertasCategorias.get("alertatotais")? "red":"" || valoresjunho.get("Total")>alertasCategorias.get("alertatotaisssaviso")? "orange":"" }">${ valoresjunho.get("Total")}</td>
                <td>${ valoresjunhovariacao.get("variação")}</td>

            </tr>



            <tr>
                <td>Julho</td>
                <td bgcolor="${valoresJul.get("Transporte")>=alertasCategorias.get("alertaTransporte")? "red":"" || valoresJul.get("Transporte")>alertasCategorias.get("alertaTransporteaviso")? "orange":"" }">${ valoresJul.get("Transporte")}</td>
                <td bgcolor="${valoresJul.get("Propinas")>=alertasCategorias.get("alertaPropinas")? "red":"" || valoresJul.get("Propinas")>alertasCategorias.get("alertaPropinasaviso")? "orange":"" }">${valoresJul.get("Propinas")}</td>
                <td bgcolor="${valoresJul.get("Alimentação")>=alertasCategorias.get("alertaAlimentacao")? "red":"" ||valoresJul.get("Alimentação")>alertasCategorias.get("alertaAlimentacaoeaviso")? "orange":"" }">${valoresJul.get("Alimentação")}</td>
                <td bgcolor="${valoresJul.get("Rendas")>=alertasCategorias.get("alertaRendas")? "red":"" ||valoresJul.get("Rendas")>alertasCategorias.get("alertaRendassaviso")? "orange":"" }">${ valoresJul.get("Rendas")}</td>
                <td>${valoresJul.get("Indifinidas")}</td>
                <td bgcolor="${valoresJul.get("Total")>=alertasCategorias.get("alertatotais")? "red":"" || valoresJul.get("Total")>alertasCategorias.get("alertatotaisssaviso")? "orange":"" }">${ valoresJul.get("Total")}</td>
                <td>${ valoresJulvariacao.get("variação")}</td>

            </tr>

            <tr>
                <td>Agosto</td>
                <td bgcolor="${valoresAgo.get("Transporte")>=alertasCategorias.get("alertaTransporte")? "red":"" || valoresAgo.get("Transporte")>alertasCategorias.get("alertaTransporteaviso")? "orange":"" }">${valoresAgo.get("Transporte")}</td>
                <td bgcolor="${valoresAgo.get("Propinas")>=alertasCategorias.get("alertaPropinas")? "red":"" || valoresAgo.get("Propinas")>alertasCategorias.get("alertaPropinasaviso")? "orange":"" }">${valoresAgo.get("Propinas")}</td>
                <td bgcolor="${valoresAgo.get("Alimentação")>=alertasCategorias.get("alertaAlimentacao")? "red":"" ||valoresAgo.get("Alimentação")>alertasCategorias.get("alertaAlimentacaoeaviso")? "orange":"" }">${valoresAgo.get("Alimentação")}</td>
                <td bgcolor="${valoresAgo.get("Rendas")>=alertasCategorias.get("alertaRendas")? "red":"" ||valoresAgo.get("Rendas")>alertasCategorias.get("alertaRendassaviso")? "orange":"" }">${valoresAgo.get("Rendas")}</td>
                <td>${valoresAgo.get("Indifinidas")}</td>
                <td bgcolor="${valoresAgo.get("Total")>=alertasCategorias.get("alertatotais")? "red":"" || valoresAgo.get("Total")>alertasCategorias.get("alertatotaisssaviso")? "orange":"" }">${valoresAgo.get("Total")}</td>
                <td >${valoresAgovariacao.get("variação")}</td>
            </tr>

            <tr>
                <td>Setembro</td>
                <td bgcolor="${valoresSet.get("Transporte")>=alertasCategorias.get("alertaTransporte")? "red":"" || valoresSet.get("Transporte")>alertasCategorias.get("alertaTransporteaviso")? "orange":"" }">${valoresSet.get("Transporte")}</td>
                <td bgcolor="${valoresSet.get("Propinas")>=alertasCategorias.get("alertaPropinas")? "red":"" || valoresSet.get("Propinas")>alertasCategorias.get("alertaPropinasaviso")? "orange":"" }">${valoresSet.get("Propinas")}</td>
                <td bgcolor="${valoresSet.get("Alimentação")>=alertasCategorias.get("alertaAlimentacao")? "red":"" ||valoresSet.get("Alimentação")>alertasCategorias.get("alertaAlimentacaoeaviso")? "orange":"" }">${valoresSet.get("Alimentação")}</td>
                <td bgcolor="${valoresSet.get("Rendas")>=alertasCategorias.get("alertaRendas")? "red":"" ||valoresSet.get("Rendas")>alertasCategorias.get("alertaRendassaviso")? "orange":"" }">${valoresSet.get("Rendas")}</td>
                <td>${valoresSet.get("Indifinidas")}</td>
                <td bgcolor="${valoresSet.get("Total")>=alertasCategorias.get("alertatotais")? "red":"" || valoresSet.get("Total")>alertasCategorias.get("alertatotaisssaviso")? "orange":"" }">${valoresSet.get("Total")}</td>
                <td>${valoresSetvariacao.get("variação")}</td>
            </tr>

            <tr>
                <td>Outobro</td>
                <td bgcolor="${valoresOut.get("Transporte")>=alertasCategorias.get("alertaTransporte")? "red":"" || valoresOut.get("Transporte")>alertasCategorias.get("alertaTransporteaviso")? "orange":"" }">${ valoresOut.get("Transporte")}</td>
                <td bgcolor="${valoresOut.get("Propinas")>=alertasCategorias.get("alertaPropinas")? "red":"" || valoresOut.get("Propinas")>alertasCategorias.get("alertaPropinasaviso")? "orange":"" }">${ valoresOut.get("Propinas")}</td>
                <td bgcolor="${valoresOut.get("Alimentação")>=alertasCategorias.get("alertaAlimentacao")? "red":"" ||valoresOut.get("Alimentação")>alertasCategorias.get("alertaAlimentacaoeaviso")? "orange":"" }">${ valoresOut.get("Alimentação")}</td>
                <td bgcolor="${valoresOut.get("Rendas")>=alertasCategorias.get("alertaRendas")? "red":"" ||valoresOut.get("Rendas")>alertasCategorias.get("alertaRendassaviso")? "orange":"" }">${ valoresOut.get("Rendas")}</td>
                <td>${valoresOut.get("Indifinidas")}</td>
                <td bgcolor="${valoresOut.get("Total")>=alertasCategorias.get("alertatotais")? "red":"" || valoresOut.get("Total")>alertasCategorias.get("alertatotaisssaviso")? "orange":"" }">${ valoresOut.get("Total")}</td>
                <td>${ valoresOutvariacao.get("variação")}</td>

            </tr>


            <tr>
                <td>Novembro</td>
                <td bgcolor="${valoresNov.get("Transporte")>=alertasCategorias.get("alertaTransporte")? "red":"" || valoresNov.get("Transporte")>alertasCategorias.get("alertaTransporteaviso")? "orange":"" }">${valoresNov.get("Transporte")}</td>
                <td bgcolor="${valoresNov.get("Propinas")>=alertasCategorias.get("alertaPropinas")? "red":"" || valoresNov.get("Propinas")>alertasCategorias.get("alertaPropinasaviso")? "orange":"" }">${ valoresNov.get("Propinas")}</td>
                <td  bgcolor="${valoresNov.get("Alimentação")>=alertasCategorias.get("alertaAlimentacao")? "red":"" ||valoresNov.get("Alimentação")>alertasCategorias.get("alertaAlimentacaoeaviso")? "orange":"" }">${ valoresNov.get("Alimentação")}</td>
                <td bgcolor="${valoresNov.get("Rendas")>=alertasCategorias.get("alertaRendas")? "red":"" ||valoresNov.get("Rendas")>alertasCategorias.get("alertaRendassaviso")? "orange":"" }">${ valoresNov.get("Rendas")}</td>
                <td>${valoresNov.get("Indifinidas")}</td>
                <td bgcolor="${valoresNov.get("Total")>=alertasCategorias.get("alertatotais")? "red":"" || valoresNov.get("Total")>alertasCategorias.get("alertatotaisssaviso")? "orange":"" }">${valoresNov.get("Total")}</td>
                <td>${valoresNovvariacao.get("variação")}</td>
            </tr>

            <tr>
                <td>Dezembro</td>
                <td bgcolor="${valoresDez.get("Transporte")>=alertasCategorias.get("alertaTransporte")? "red":"" || valoresDez.get("Transporte")>alertasCategorias.get("alertaTransporteaviso")? "orange":"" }">${ valoresDez.get("Transporte")}</td>
                <td bgcolor="${valoresDez.get("Propinas")>=alertasCategorias.get("alertaPropinas")? "red":"" || valoresDez.get("Propinas")>alertasCategorias.get("alertaPropinasaviso")? "orange":"" }">${ valoresDez.get("Propinas")}</td>
                <td  bgcolor="${valoresDez.get("Alimentação")>=alertasCategorias.get("alertaAlimentacao")? "red":"" ||valoresDez.get("Alimentação")>alertasCategorias.get("alertaAlimentacaoeaviso")? "orange":"" }">${ valoresDez.get("Alimentação")}</td>
                <td bgcolor="${valoresDez.get("Rendas")>=alertasCategorias.get("alertaRendas")? "red":"" ||valoresDez.get("Rendas")>alertasCategorias.get("alertaRendassaviso")? "orange":"" }">${ valoresDez.get("Rendas")}</td>
                <td>${valoresDez.get("Indifinidas")}</td>
                <td bgcolor="${valoresDez.get("Total")>=alertasCategorias.get("alertatotais")? "red":"" || valoresDez.get("Total")>alertasCategorias.get("alertatotaisssaviso")? "orange":"" }">${valoresDez.get("Total")}</td>
                <td>${valoresDezvariacao.get("variação")}</td>
            </tr>

            <tr>
                <td>Total</td>
                <td>${ TotaisCategorias.get("totalTransporte")}</td>
                <td>${ TotaisCategorias.get("totalPropinas")}</td>
                <td>${ TotaisCategorias.get("totalAlimentação")}</td>
                <td>${ TotaisCategorias.get("totalRendas")}</td>
                <td>${ TotaisCategorias.get("totalIdifinidas")}</td>
                <td>${ TotaisCategorias.get("totaistais")}</td>
                <td>${ valoresDezvariacao.get("totalVariação")}</td>
            </tr>

        </table>

    </tiles:putAttribute>
</tiles:insertDefinition>
