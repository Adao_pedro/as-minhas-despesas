<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">

        <table border="1" align="center">
            <tr>
                <th>Categoria</th>
                <th>localizacao</th>
                <th>valor1</th>
                <th>Data</th>
                <th>Recorrencia</th>
                <th>Editar</th>
                <th>Apagar</th>

            </tr>

                <tr>
                    <td> ${despesa.categoriaName}</td>
                    <td>${despesa.localizacao}</td>
                    <td>${despesa.valor}</td>
                    <td>${despesa.dataName}</td>
                    <td>${despesa.recorrencia}</td>
                    <td><a href="/edit/${despesa.id}">Editar</a></td>
                    <td>
                        <form action="/delete/${despesa.id}" method="post">
                            <input type="submit" value="Apagar" />
                        </form>
                    </td>


                </tr>

        </table>

    </tiles:putAttribute>
</tiles:insertDefinition>