﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">

        <form:form method="POST" modelAttribute="despesaForm" action="/form">

            <form:hidden path="id"/>
            <form:hidden path="login"/>

            <form:label path="categoria">Categorias</form:label>
            <form:select path="categoria" label="categoria">
                <form:option value="" label="--- Select ---"/>
                <form:options items="${categoriasMap}"/>
            </form:select>
            <form:errors path="categoria" cssClass="error"/><br/>


            <form:label path="descricao">descricao</form:label>
            <form:input path="descricao" label="descricao" />
            <form:errors path="descricao" cssClass="error"/><br/>

            <form:label path="valor" >valor</form:label>
            <form:input path="valor" label="valor"/>
            <form:errors path="valor" cssClass="error"/><br/>

            <form:label path="localizacao">localizacao</form:label>
            <form:input path="localizacao" label="localizacao" />
            <form:errors path="localizacao" cssClass="error"/><br/>


            <form:label path="data">Data</form:label>
            <select name="data" class="frmCmb1">
                <option value="1">Janeiro</option>
                <option value="2">Fevereiro</option>
                <option value="3">Março</option>
                <option value="4">Abril</option>
                <option value="5">Maio</option>
                <option value="6">Junho</option>
                <option value="7">Julho</option>
                <option value="8">Agosto</option>
                <option value="9">Setembro</option>
                <option value="10">Outubro</option>
                <option value="11">Novembro</option>
                <option value="12">Dezembro</option>
            </select>
            <form:errors path="data" cssClass="error"/><br/>



            <form:label path="recorrencia">Recorrencia</form:label>
            <select name="recorrencia" class="frmCmb1">
                <option value="Pontual">Pontual</option>
                <option value="Mensal">Mensal</option>
            </select>
            <form:errors path="recorrencia" cssClass="error"/><br/>


            <input type="submit" name="Gravar"/>
        </form:form>
        <p align="center">
            <a href="/upload/">Upload </a>
        </p>
    </tiles:putAttribute>
</tiles:insertDefinition>
